from django.apps import AppConfig


class ChebiModelsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'chebi_models'
