from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from chebi_models.models.managers import PublicCompoundsManager


class BaseModel(models.Model):
    # TODO: Switch to history table in future and discard this.
    # keeping these as CharField to allow import of historic data. Switch to FK in future
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    created_by = models.CharField(max_length=256, null=True)
    modified_on = models.DateTimeField(auto_now=True, null=True)
    modified_by = models.CharField(max_length=256, null=True)
    deleted_on = models.DateTimeField(auto_now=True, null=True)
    deleted_by = models.CharField(max_length=256, null=True)

    class Meta:
        abstract = True


class Status(models.Model):
    code = models.CharField(max_length=1)
    name = models.CharField(max_length=32)

    def __str__(self):
        return f"Status - {self.code} - {self.name}"

    class Meta:
        db_table = 'status'


class Source(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return f"Source - {self.name}"

    class Meta:
        db_table = 'source'


class DatabaseAccessionType(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return f"Database Accession Type - {self.name}"

    class Meta:
        db_table = 'database_accession_type'


class Compound(BaseModel):
    name = models.CharField(max_length=2048, null=True)
    status = models.ForeignKey(Status, on_delete=models.PROTECT, related_name='+')
    source = models.ForeignKey(Source, on_delete=models.PROTECT, related_name='+')
    parent = models.ForeignKey("self", on_delete=models.SET_NULL, null=True)
    merge_type = models.CharField(max_length=256, null=True)
    chebi_accession = models.CharField(max_length=25)
    definition = models.TextField(null=True)
    ascii_name = models.CharField(max_length=1024)
    stars = models.PositiveSmallIntegerField(validators=[MaxValueValidator(3), MinValueValidator(1)], default=1)
    # TODO: created_by is CharField in old schema, will make it ForeignKey to user in future
    created_by = models.CharField(max_length=50)
    # Managers
    objects = PublicCompoundsManager()
    all_objects = models.Manager()

    class Meta:
        db_table = 'compounds'


class CompoundOrigins(BaseModel):
    compound = models.ForeignKey(Compound, on_delete=models.CASCADE, related_name='compound_origins')
    species_text = models.CharField(max_length=1024, null=True)
    species_accession = models.CharField(max_length=30, null=True)
    # Future Improvement: Can we make component_text a choice field?
    component_text = models.CharField(max_length=1024, null=True)
    component_accession = models.CharField(max_length=30, null=True)
    strain_text = models.CharField(max_length=1024, null=True)
    strain_accession = models.CharField(max_length=30, null=True)
    source_type = models.ForeignKey(Source, on_delete=models.PROTECT)
    source_accession = models.CharField(max_length=1024, null=True)
    comments = models.TextField(null=True)
    status = models.ForeignKey(Status, on_delete=models.PROTECT, related_name='+')

    class Meta:
        db_table = 'compound_origins'


class URLStore(BaseModel):
    code = models.CharField(max_length=30, unique=True)
    database_name = models.CharField(max_length=30)
    url = models.CharField(max_length=600)

    class Meta:
        db_table = 'url_store'


class DatabaseAccession(BaseModel):
    compound = models.ForeignKey(Compound, related_name='database_accessions', on_delete=models.CASCADE)
    accession_number = models.CharField(max_length=255)
    type = models.ForeignKey(DatabaseAccessionType, on_delete=models.PROTECT, related_name='+')
    status = models.ForeignKey(Status, on_delete=models.PROTECT, related_name='+')
    source = models.ForeignKey(Source, on_delete=models.PROTECT, related_name='+')
    url_abbr = models.ForeignKey(URLStore, on_delete=models.DO_NOTHING, null=True)

    class Meta:
        db_table = 'database_accession'


class Names(BaseModel):
    compound = models.ForeignKey(Compound, related_name='names', on_delete=models.CASCADE)
    # Find max length of name in database, 4000 seems too much
    name = models.CharField(max_length=4000)
    # TODO: Discuss for mapping with ETL
    type = models.CharField(max_length=25)
    status = models.ForeignKey(Status, on_delete=models.PROTECT, related_name='+')
    source = models.ForeignKey(Source, on_delete=models.PROTECT, related_name='+')
    url_abbr = models.ForeignKey(URLStore, on_delete=models.DO_NOTHING, null=True)
    adapted = models.BooleanField()
    language_code = models.CharField(max_length=20, null=True)
    # TODO: Find max length of this
    ascii_name = models.CharField(max_length=4000)

    class Meta:
        db_table = 'names'


class DataSource(BaseModel):
    name = models.CharField(max_length=512, unique=True)
    description = models.CharField(max_length=4000)

    class Meta:
        db_table = 'datasource'


class Reference(BaseModel):
    compound = models.ForeignKey(Compound, related_name='references', on_delete=models.CASCADE)
    reference_id = models.CharField(max_length=60)
    datasource = models.ForeignKey(DataSource, on_delete=models.DO_NOTHING, null=True)
    location_in_ref = models.CharField(max_length=90, null=True)
    autogenerated = models.BooleanField(null=True)
    url_abbr = models.ForeignKey(URLStore, on_delete=models.DO_NOTHING, null=True)
    accession_number = models.CharField(max_length=60)
    reference_name = models.CharField(max_length=1024, null=True)

    class Meta:
        db_table = 'reference'


class RelationType(BaseModel):
    code = models.CharField(max_length=60, unique=True)
    allow_cycles = models.BooleanField()
    description = models.CharField(max_length=200)

    class Meta:
        db_table = 'relation_type'


class Relation(BaseModel):
    relation_type = models.ForeignKey(RelationType, on_delete=models.DO_NOTHING)
    init = models.ForeignKey(Compound, on_delete=models.CASCADE, related_name='outgoing_relations')
    final = models.ForeignKey(Compound, on_delete=models.CASCADE, related_name='incoming_relations')
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    evidence_dbaccid = models.CharField(max_length=255, null=True)

    class Meta:
        db_table = 'relation'


class Structure(BaseModel):
    compound = models.ForeignKey(Compound, related_name='structures', on_delete=models.CASCADE)
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    structure = models.TextField()
    type = models.CharField(max_length=25)
    dimension = models.CharField(max_length=25)
    default_structure = models.BooleanField()

    class Meta:
        db_table = 'structures'


class Comments(BaseModel):
    compound = models.ForeignKey(Compound, on_delete=models.CASCADE, related_name='comments')
    comment = models.TextField(null=True)  # remove null True after checking later
    author_name = models.CharField(max_length=100, null=True)
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    datatype = models.CharField(max_length=50)
    # What does data_type_id mean?
    datatype_id = models.IntegerField()

    class Meta:
        db_table = 'comments'


class ChemicalData(BaseModel):
    compound = models.ForeignKey(Compound, on_delete=models.CASCADE, related_name='chemical_data')
    status = models.ForeignKey(Status, on_delete=models.PROTECT)
    source = models.ForeignKey(Source, on_delete=models.PROTECT)
    # TODO: Verify these field limits
    formula = models.TextField()
    charge = models.CharField(max_length=12)
    mass = models.CharField(max_length=56)
    monoisotopic_mass = models.CharField(max_length=56)

    class Meta:
        db_table = 'chemical_data'
