from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    # TODO: Migrate user table
    class Meta:
        db_table = 'user'
