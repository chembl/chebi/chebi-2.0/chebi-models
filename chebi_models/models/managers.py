from django.db import models


class PublicCompoundsManager(models.Manager):
    """
    Used as default Manager for Compounds
    This manager defines the base query for ChEBI public compounds i.e compounds having 1 or more stars
    """
    def get_queryset(self):
        return super().get_queryset().filter(stars__gte=1)

