=====
ChEBI Models
=====

ChEBI Models is a Django app that contains all models of ChEBI. It is used as a package in other Chebi services.

Detailed documentation to follow.
